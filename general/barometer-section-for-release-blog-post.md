# How to prepare Barometer section for the Release blog post

The Barometer section describes how to upgrade GitLab to the new version on the [Release post](https://about.gitlab.com/handbook/marketing/blog/release-posts/#release-posts). This is added by the Release Manager days in advance before the 22nd. Normally the Release Post Manager will ping the Release Managers so they can start working on this section. You can use the template presented under the 'Upgrade barometer' section in the [release post data file](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/templates/blog/YYYY_MM_DD_gitlab_x_y_released.yml).

The barometer section should include the following info:

- How should the upgrade be done?
- Do we expect downtime? If so, how long will it take?
- States timing details about migrations, post migrations and background migrations. Remember that the timings are posted in the #announcements channel on slack.
- Are there any special cases, something important to note?

Note: It's important to have this section added with all the others, but, of course, it can be updated later if necessary.

### Examples:

- [Barometer section for 11.1](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/13064)
- [Barometer section for 11.0](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/12526)
- [Barometer section for 10.8](ttps://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/11716)
